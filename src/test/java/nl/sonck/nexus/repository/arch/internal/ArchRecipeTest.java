/*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
 */
package nl.sonck.nexus.repository.arch.internal;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.sonatype.goodies.testsupport.TestSupport;
import nl.sonck.nexus.repository.arch.internal.hosted.ArchHostedRecipe;
import org.sonatype.nexus.repository.types.ProxyType;
import org.sonatype.nexus.repository.view.handlers.HighAvailabilitySupportChecker;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * @since 3.17
 */
public class ArchRecipeTest
    extends TestSupport
{
  @Mock
  ArchFormat format;

  @Mock
  HighAvailabilitySupportChecker highAvailabilitySupportChecker;

  ArchHostedRecipe hostedRecipe;

  final String ARCH_NAME = "arch";

  @Before
  public void setUp() {
    hostedRecipe = new ArchHostedRecipe(highAvailabilitySupportChecker, new ProxyType(), format);
    when(format.getValue()).thenReturn(ARCH_NAME);
  }

  @Test
  public void enabledByDefault_ArchHostedRepository() {
    when(highAvailabilitySupportChecker.isSupported(ARCH_NAME)).thenReturn(true);
    assertThat(hostedRecipe.isFeatureEnabled(), is(equalTo(true)));
    verify(highAvailabilitySupportChecker).isSupported(ARCH_NAME);
  }

  @Test
  public void disabledIfNexusIsClusteredAndArchNotCluster_ArchHostedRepository() {
    when(highAvailabilitySupportChecker.isSupported(ARCH_NAME)).thenReturn(false);
    assertThat(hostedRecipe.isFeatureEnabled(), is(equalTo(false)));
    verify(highAvailabilitySupportChecker).isSupported(ARCH_NAME);
  }

}
