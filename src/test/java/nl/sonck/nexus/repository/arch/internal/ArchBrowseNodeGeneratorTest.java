/*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
 */
package nl.sonck.nexus.repository.arch.internal;

import org.junit.Test;
import org.mockito.Mock;
import org.sonatype.nexus.common.collect.NestedAttributesMap;
import org.sonatype.nexus.repository.browse.BrowsePaths;
import org.sonatype.nexus.repository.browse.BrowseTestSupport;
import org.sonatype.nexus.repository.storage.Asset;
import org.sonatype.nexus.repository.storage.Component;

import java.util.List;

import static java.util.Arrays.asList;
import static org.mockito.Mockito.when;

/**
 * @since 3.17
 */
public class ArchBrowseNodeGeneratorTest
    extends BrowseTestSupport
{
  private ArchBrowseNodeGenerator generator = new ArchBrowseNodeGenerator();

  @Mock
  Asset asset;

  @Mock
  NestedAttributesMap attrs;

  @Test
  public void computeComponentPath() {
    Component component = createComponent("nano", "amd64", "1.0.0");

    when(attrs.get("package_repo", String.class)).thenReturn("main");

    when(asset.formatAttributes()).thenReturn(attrs);

    List<BrowsePaths> paths = generator.computeComponentPaths(asset, component);
    assertPaths(asList("packages", "main", "nano"), paths, true);
  }
}
