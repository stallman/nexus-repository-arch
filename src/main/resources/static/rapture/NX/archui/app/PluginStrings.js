/*
 * Nexus APT plugin.
 * 
 * Copyright (c) 2016-Present Michael Poindexter.
 * 
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * "Sonatype" and "Sonatype Nexus" are trademarks of Sonatype, Inc.
 */
/*global Ext, NX*/

/**
 * Apt plugin strings.
 */
Ext.define('NX.archui.app.PluginStrings', {
  '@aggregate_priority': 90,

  singleton: true,
  requires: [
    'NX.I18n'
  ],

  keys: {
    Repository_Facet_ArchFacet_Title: 'Arch Settings',
    Repository_Facet_ArchFacet_RepoName_FieldLabel: 'Repository Name',
    Repository_Facet_ArchFacet_RepoName_HelpText: 'Repository name to fetch, for example extra',
    Repository_Facet_ArchFacet_Arch_FieldLabel: 'Architecture',
    Repository_Facet_ArchFacet_Arch_HelpText: 'The main architecture of this repository',
    Repository_Facet_ArchSigningFacet_Keypair_FieldLabel: 'Signing Key',
    Repository_Facet_ArchSigningFacet_Keypair_HelpText: 'PGP signing key pair (armored private key eg. gpg --export-secret-key --armor <Name or ID>)',
    Repository_Facet_ArchSigningFacet_Passphrase_FieldLabel: 'Passphrase',
  }
}, function(self) {
  NX.I18n.register(self);
});
