/*
 * Nexus APT plugin.
 * 
 * Copyright (c) 2016-Present Michael Poindexter.
 * 
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * "Sonatype" and "Sonatype Nexus" are trademarks of Sonatype, Inc.
 */

/**
 * Configuration specific to apt repositories.
 */
Ext.define('NX.archui.view.repository.facet.ArchFacet', {
  extend: 'Ext.form.FieldContainer',
  alias: 'widget.nx-archui-repository-arch-facet',
  requires: [
    'NX.I18n'
  ],
  /**
   * @override
   */
  initComponent: function() {
    var me = this;

    me.items = [
      {
        xtype: 'fieldset',
        cls: 'nx-form-section',
        title: NX.I18n.get('Repository_Facet_ArchFacet_Title'),
        items: [
          {
            xtype:'textfield',
            name: 'attributes.arch.reponame',
            fieldLabel: NX.I18n.get('Repository_Facet_ArchFacet_RepoName_FieldLabel'),
            helpText: NX.I18n.get('Repository_Facet_ArchFacet_RepoName_HelpText'),
            allowBlank: false
          },
          {
            xtype: 'textfield',
            name: 'attributes.arch.architecture',
            fieldLabel: NX.I18n.get('Repository_Facet_ArchFacet_Arch_FieldLabel'),
            helpText: NX.I18n.get('Repository_Facet_ArchFacet_Arch_HelpText'),
            value: false
          }
        ]
      }
    ];

    me.callParent();
  }

});
