/*
 * Nexus APT plugin.
 * 
 * Copyright (c) 2016-Present Michael Poindexter.
 * 
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * "Sonatype" and "Sonatype Nexus" are trademarks of Sonatype, Inc.
 */

/**
 * Configuration specific to apt repositories.
 */
Ext.define('NX.archui.view.repository.facet.ArchSigningFacet', {
  extend: 'Ext.form.FieldContainer',
  alias: 'widget.nx-archui-repository-archsigning-facet',
  requires: [
    'NX.I18n'
  ],
  /**
   * @override
   */
  initComponent: function() {
    var me = this;

    me.items = [
      {
        xtype: 'fieldset',
        cls: 'nx-form-section',
        title: NX.I18n.get('Repository_Facet_ArchFacet_Title'),
        items: [
          {
            xtype:'textareafield',
            name: 'attributes.archSigning.keypair',
            fieldLabel: NX.I18n.get('Repository_Facet_ArchSigningFacet_Keypair_FieldLabel'),
            helpText: NX.I18n.get('Repository_Facet_ArchSigningFacet_Keypair_HelpText'),
            allowBlank: false,
            grow: true
          },
          {
            xtype:'nx-password',
            name: 'attributes.archSigning.passphrase',
            fieldLabel: NX.I18n.get('Repository_Facet_ArchSigningFacet_Passphrase_FieldLabel'),
            allowBlank: true
          }
        ]
      }
    ];

    me.callParent();
  }

});
