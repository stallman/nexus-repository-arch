package nl.sonck.nexus.repository.arch.internal.hosted;

import com.google.common.hash.HashCode;
import com.orientechnologies.orient.core.record.impl.ODocument;
import nl.sonck.nexus.repository.arch.internal.ArchPackageParser;
import nl.sonck.nexus.repository.arch.internal.FacetHelper;
import nl.sonck.nexus.repository.arch.internal.arch.InfoFile;
import nl.sonck.nexus.repository.arch.internal.arch.MetaFile;
import nl.sonck.nexus.repository.arch.internal.arch.PackageInfo;
import org.apache.commons.compress.archivers.tar.TarArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.lang3.tuple.Pair;
import org.sonatype.nexus.common.hash.HashAlgorithm;
import org.sonatype.nexus.orient.entity.AttachedEntityHelper;
import org.sonatype.nexus.repository.Facet;
import org.sonatype.nexus.repository.FacetSupport;
import org.sonatype.nexus.repository.IllegalOperationException;
import nl.sonck.nexus.repository.arch.ArchFacet;
import nl.sonck.nexus.repository.arch.internal.ArchMimeTypes;
import org.sonatype.nexus.repository.storage.*;
import org.sonatype.nexus.repository.transaction.TransactionalStoreBlob;
import org.sonatype.nexus.repository.transaction.TransactionalStoreMetadata;
import org.sonatype.nexus.repository.view.Content;
import org.sonatype.nexus.repository.view.Payload;
import org.sonatype.nexus.repository.view.payloads.StreamPayload;
import org.sonatype.nexus.transaction.UnitOfWork;

import javax.inject.Named;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.singletonList;
import static org.sonatype.nexus.common.hash.HashAlgorithm.MD5;
import static org.sonatype.nexus.common.hash.HashAlgorithm.SHA256;
import static org.sonatype.nexus.repository.storage.AssetEntityAdapter.P_ASSET_KIND;
import static org.sonatype.nexus.repository.storage.MetadataNodeEntityAdapter.P_BUCKET;
import static org.sonatype.nexus.repository.storage.MetadataNodeEntityAdapter.P_NAME;

@Named
@Facet.Exposed
public class ArchHostedFacet
        extends FacetSupport {
    private static final String P_ARCHITECTURE = "architecture";
    private static final String P_PACKAGE_REPO = "package_repo";
    private static final String P_PACKAGE_NAME = "package_name";
    private static final String P_PACKAGE_VERSION = "package_version";
    private static final String P_PACKAGE_INFORMATION = "package_information";

    private static final String SELECT_HOSTED_ASSETS =
            "SELECT " +
                    "name, " +
                    "attributes.arch.package_repo AS package_repo, " +
                    "attributes.arch.package_version AS package_version, " +
                    "attributes.arch.package_name AS package_name, " +
                    "attributes.arch.package_information AS package_information, " +
                    "attributes.arch.architecture AS architecture " +
                    "FROM asset " +
                    "WHERE bucket=:bucket " +
                    "AND attributes.arch.asset_kind=:asset_kind";

    @TransactionalStoreBlob
    public Asset ingestAsset(final String repoName, final Payload body) throws IOException {
        StorageFacet storageFacet = facet(StorageFacet.class);
        try (TempBlob tempBlob = storageFacet.createTempBlob(body, FacetHelper.hashAlgorithms)) {
            InfoFile info = ArchPackageParser.parsePackage(tempBlob);
            if (info == null) {
                throw new IllegalOperationException("Invalid Arch package supplied");
            }
            return ingestAsset(repoName, info, tempBlob, body.getSize(), body.getContentType());
        }
    }

    @TransactionalStoreBlob
    protected Asset ingestAsset(final String repoName, final InfoFile infoFile, final TempBlob body, final long size, final String contentType) throws IOException {
        ArchFacet archFacet = getRepository().facet(ArchFacet.class);
        StorageTx tx = UnitOfWork.currentTx();
        Bucket bucket = tx.findBucket(getRepository());

        PackageInfo info = new PackageInfo(infoFile);
        String name = info.getPackageName();
        String version = info.getVersion();
        String architecture = info.getArchitecture();

        // Find old package versions of the same thing
        Map<String, Object> sqlParams = new HashMap<>();
        sqlParams.put(P_BUCKET, AttachedEntityHelper.id(bucket));
        sqlParams.put(P_PACKAGE_NAME, name);
        sqlParams.put(P_PACKAGE_REPO, repoName);

        Iterable<ODocument> browse = tx.browse("SELECT name, " +
                "attributes.arch.package_version AS package_version " +
                "FROM asset " +
                "WHERE bucket=:bucket " +
                "AND attributes.arch.package_name=:package_name " +
                "AND attributes.arch.package_repo=:package_repo", sqlParams);

        for(ODocument item : browse) {
            // Prevent useless deletion when same version is going to be added
            if(!version.equals(item.field(P_PACKAGE_VERSION, String.class)))
                archFacet.delete(item.field(P_NAME, String.class));
        }

        String assetPath = FacetHelper.buildAssetPath(repoName, archFacet, name, version, architecture);
        String filename = FacetHelper.buildAssetName(name, version, architecture);

        Content content = archFacet.put(
                assetPath,
                new StreamPayload(body::get, size, contentType),
                info
        );

        Asset asset = Content.findAsset(tx, bucket, content);
        String packageInfo =
                buildPackageInfo(infoFile, asset.size(), asset.getChecksums(FacetHelper.hashAlgorithms), filename);
        asset.formatAttributes().set(P_ARCHITECTURE, architecture);
        asset.formatAttributes().set(P_PACKAGE_REPO, repoName);
        asset.formatAttributes().set(P_PACKAGE_NAME, name);
        asset.formatAttributes().set(P_PACKAGE_VERSION, version);
        asset.formatAttributes().set(P_PACKAGE_INFORMATION, packageInfo);
        asset.formatAttributes().set(P_ASSET_KIND, "ARCH");
        tx.saveAsset(asset);


        rebuildIndexes(singletonList(new ArchHostedFacet.AssetChange(AssetAction.ADDED, asset)));
        return asset;
    }

    public void rebuildIndexes() throws IOException {
        rebuildIndexes(Collections.emptyList());
    }

    @TransactionalStoreMetadata
    public void rebuildIndexes(final List<AssetChange> changes) throws IOException {
        StorageTx tx = UnitOfWork.currentTx();
        ArchFacet archFacet = getRepository().facet(ArchFacet.class);
        Bucket bucket = tx.findBucket(getRepository());

        StringBuilder sha256Builder = new StringBuilder();
        StringBuilder md5Builder = new StringBuilder();
        try (TarTempFileStorage dbTar = buildDbIndex(tx, bucket, changes)) {
            for(Map.Entry<String,TarTempFileStorage.FileMetadata> entry : dbTar.getFiles().entrySet()) {
                TarTempFileStorage.FileMetadata meta = entry.getValue();

                Content dbContent = archFacet.put(
                        dbIndexName(entry.getKey(), archFacet, ""),
                        new StreamPayload(meta.supplier(), meta.size(), ArchMimeTypes.GZIP)
                );
                Content dbGzContent = archFacet.put(
                        dbIndexName(entry.getKey(), archFacet, ".tar.gz"),
                        new StreamPayload(meta.supplier(), meta.size(), ArchMimeTypes.GZIP)
                );
            }
        }

    }

    private TarTempFileStorage buildDbIndex(final StorageTx tx, final Bucket bucket, final List<AssetChange> changes) throws IOException {
        TarTempFileStorage result = new TarTempFileStorage();
        Map<String, TarArchiveOutputStream> streams = new HashMap<>();

        try{
            Map<String, Object> sqlParams = new HashMap<>();
            sqlParams.put(P_BUCKET, AttachedEntityHelper.id(bucket));
            sqlParams.put(P_ASSET_KIND, "ARCH");

            Set<Pair<String, String>> excludeNames = changes.stream().map(change -> Pair.of(
                    change.asset.formatAttributes().get(P_PACKAGE_NAME, String.class),
                    change.asset.formatAttributes().get(P_PACKAGE_REPO, String.class))).collect(Collectors.toSet());

            Iterable<ODocument> browse = tx.browse(SELECT_HOSTED_ASSETS, sqlParams);

            for (ODocument document : browse) {
                String name = document.field(P_PACKAGE_NAME, String.class);
                String version = document.field(P_PACKAGE_VERSION, String.class);
                String repoName = document.field(P_PACKAGE_REPO, String.class);

                log.warn("Handling pkg {} ver {} repo {}", name, version, repoName);

                if (!excludeNames.contains(Pair.of(name, repoName))) {
                    TarArchiveOutputStream stream = streams.computeIfAbsent(repoName, result::openOutput);
                    String info = document.field(P_PACKAGE_INFORMATION, String.class);
                    byte[] content = info.getBytes(UTF_8);

                    TarArchiveEntry entry = new TarArchiveEntry(String.format("%s-%s/desc", name, version));

                    log.warn("Writing entry {} with size {} using stream {}", entry.getName(), info.length(), stream);

                    entry.setSize(content.length);
                    stream.putArchiveEntry(entry);
                    stream.write(content);
                    stream.closeArchiveEntry();
                }

            }

            List<Asset> addAsset = changes.stream().filter(change -> change.action == AssetAction.ADDED)
                    .map(change -> change.asset).collect(Collectors.toList());

            for (Asset asset : addAsset) {
                String name = asset.formatAttributes().get(P_PACKAGE_NAME, String.class);
                String version = asset.formatAttributes().get(P_PACKAGE_VERSION, String.class);
                String info = asset.formatAttributes().get(P_PACKAGE_INFORMATION, String.class);
                String repoName = asset.formatAttributes().get(P_PACKAGE_REPO, String.class);

                TarArchiveOutputStream stream = streams.computeIfAbsent(repoName, result::openOutput);

                byte[] content = info.getBytes(UTF_8);

                TarArchiveEntry entry = new TarArchiveEntry(String.format("%s-%s/desc", name, version));

                entry.setSize(content.length);
                stream.putArchiveEntry(entry);
                stream.write(content);
                stream.closeArchiveEntry();

            }
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        } finally {
            for(TarArchiveOutputStream stream : streams.values()) {
                log.warn("Finalizing stream {}", stream);
                stream.close();
            }
        }
        return result;
    }

    private String buildPackageInfo(final InfoFile infoFile, final long size, final Map<HashAlgorithm, HashCode> hashes, final String filename) {
        MetaFile modified = infoFile.toMetaFile()
                .withFields(Arrays.asList(
                        new MetaFile.MetaField(PackageInfo.Field.FILENAME, filename),
                        new MetaFile.MetaField(PackageInfo.Field.CSIZE, Long.toString(size)),
                        new MetaFile.MetaField(PackageInfo.Field.MD5SUM, hashes.get(MD5).toString()),
                        new MetaFile.MetaField(PackageInfo.Field.SHA256SUM, hashes.get(SHA256).toString())
                        )
                );
        return modified.toString();
    }

    private String dbIndexName(final String repoName, final ArchFacet archFacet, final String ext) {
        return repoName + "/os/" + archFacet.getArchitecture() + "/" + repoName + ".db" + ext;
    }

    public enum AssetAction {
        ADDED, REMOVED
    }

    public static class AssetChange {
        final AssetAction action;

        public final Asset asset;

        public AssetChange(final AssetAction action, final Asset asset) {
            super();
            this.action = action;
            this.asset = asset;
        }
    }
}
