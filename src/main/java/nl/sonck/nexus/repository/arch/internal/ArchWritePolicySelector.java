package nl.sonck.nexus.repository.arch.internal;

import org.sonatype.nexus.repository.storage.Asset;
import org.sonatype.nexus.repository.storage.WritePolicy;
import org.sonatype.nexus.repository.storage.WritePolicySelector;

public class ArchWritePolicySelector implements WritePolicySelector {
    @Override
    public WritePolicy select(final Asset asset, final WritePolicy configured) {
        if(WritePolicy.ALLOW_ONCE == configured) {
            String name = asset.name();

            if(name.endsWith(".pkg.tar.xz")) {
                return WritePolicy.ALLOW_ONCE;
            } else {
                return WritePolicy.ALLOW;
            }
        }
        return configured;
    }
}
