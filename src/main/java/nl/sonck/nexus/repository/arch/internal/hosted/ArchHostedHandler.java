/*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
 */
package nl.sonck.nexus.repository.arch.internal.hosted;

import org.sonatype.goodies.common.ComponentSupport;
import nl.sonck.nexus.repository.arch.ArchFacet;
import org.sonatype.nexus.repository.http.HttpResponses;
import org.sonatype.nexus.repository.view.Content;
import org.sonatype.nexus.repository.view.Context;
import org.sonatype.nexus.repository.view.Handler;
import org.sonatype.nexus.repository.view.Response;

import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;

import static org.sonatype.nexus.repository.http.HttpMethods.*;

/**
 * @since 3.17
 */
@Named
@Singleton
public class ArchHostedHandler
    extends ComponentSupport
    implements Handler
{
  @Override
  public Response handle(final Context context) throws Exception {
    String path = context.getRequest().getPath().substring(1);
    String method = context.getRequest().getAction();

    ArchFacet archFacet = context.getRepository().facet(ArchFacet.class);
    ArchHostedFacet hostedFacet = context.getRepository().facet(ArchHostedFacet.class);

    switch (method) {
      case GET:
      case HEAD:
        return doGet(path, archFacet);
      case POST:
        return doPost(context, path, method, hostedFacet);
      default:
        return HttpResponses.methodNotAllowed(method, GET, HEAD, POST);
    }
  }

  private Response doPost(final Context context,
                          final String path,
                          final String method,
                          final ArchHostedFacet hostedFacet) throws IOException
  {
    String[] items;
    if ("rebuild-indexes".equals(path)) {
      hostedFacet.rebuildIndexes();
      return HttpResponses.ok();
    }
    else if ((items = path.split("/")).length == 1) {
      String repoName = items[0];
      hostedFacet.ingestAsset(repoName, context.getRequest().getPayload());
      return HttpResponses.created();
    }
    else {
      log.warn("Unrecognized path: {}", path);
      return HttpResponses.methodNotAllowed(method, GET, HEAD);
    }
  }

  private Response doGet(final String path, final ArchFacet archFacet) throws IOException {
    Content content = archFacet.get(path);
    if (content == null) {
      return HttpResponses.notFound(path);
    }
    return HttpResponses.ok(content);
  }
}
