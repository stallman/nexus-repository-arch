package nl.sonck.nexus.repository.arch.internal.arch;

import java.io.IOException;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class InfoFile {
    private final List<InfoField> fields;

    public InfoFile withFields(List<InfoField> updateFields) {
        Map<PackageInfo.Field, InfoField> index = updateFields.stream().collect(Collectors.toMap(f -> f.key, f -> f));
        return new InfoFile(Stream
                .concat(fields.stream().filter(f -> !index.containsKey(f.key)), updateFields.stream())
                .collect(Collectors.toList())
        );
    }

    static class Builder {
        private List<InfoField> fields;

        private Builder(final List<InfoField> fields) {
            super();
            this.fields = fields;
        }

        public Builder removeFields(Predicate<InfoField> p) {
            fields = fields.stream().filter(p).collect(Collectors.toList());
            return this;
        }

        Builder addField(InfoField p) {
            fields.add(p);
            return this;
        }

        Builder replaceField(Predicate<InfoField> predicate, InfoField p) {
            fields = Stream
                    .concat(fields.stream().filter(predicate), Stream.of(p))
                    .collect(Collectors.toList());
            return this;
        }

        Builder transformParagraphs(Predicate<InfoField> predicate, Function<InfoField, InfoField> transform) {
            fields = fields.stream()
                    .filter(predicate)
                    .map(transform)
                    .collect(Collectors.toList());
            return this;
        }

        InfoFile build() {
            return new InfoFile(fields);
        }
    }

    public InfoFile(final List<InfoField> fields) {
        super();
        this.fields = new ArrayList<>(fields);
    }

    public Builder builder() {
        return new Builder(new ArrayList<>(fields));
    }

    public List<InfoField> getField(PackageInfo.Field key) {
        return fields.stream().filter(f -> key.equals(f.key)).collect(Collectors.toList());
    }

    public MetaFile toMetaFile(){
        Map<PackageInfo.Field, StringBuilder> result = new HashMap<>();
        for(InfoField info : fields) {
            if(!info.key.hasMeta())
                continue;

            StringBuilder value = result.computeIfAbsent(info.key, (String) -> new StringBuilder());
            if(value.length() > 0) {
                value.append('\n');
            }
            value.append(info.value);
        }

        return new MetaFile(result.entrySet().stream()
                .map(e -> new MetaFile.MetaField(e.getKey(), e.getValue().toString()))
                .collect(Collectors.toList())
        );
    }

    @Override
    public String toString() {
        return fields.stream().map(InfoField::toString).collect(Collectors.joining("\n"));
    }

    public static class InfoField {
        public final PackageInfo.Field key;

        public final String value;

        public InfoField(final PackageInfo.Field key, final String value) {
            this.key = key;
            this.value = value;
        }

        public InfoField(final String key, final String value) throws IOException {
            this(PackageInfo.Field.fromInfo(key).orElseThrow(() -> new IOException("Invalid field: " + key)), value);
        }

        public String foldedValue() {
            return Arrays.stream(value.split("\n"))
                    .map(String::trim)
                    .collect(Collectors.joining());
        }

        public List<String> listValue() {
            return Arrays.asList(value.trim().split("\\s+"));
        }

        @Override
        public String toString() {
            return String.format("%s = %s", key.info, value);
        }
    }
}