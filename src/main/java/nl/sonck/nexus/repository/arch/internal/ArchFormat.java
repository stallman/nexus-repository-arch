package nl.sonck.nexus.repository.arch.internal;

import org.sonatype.nexus.repository.Format;

import javax.inject.Named;
import javax.inject.Singleton;

@Named(ArchFormat.NAME)
@Singleton
public class ArchFormat
extends Format {
    public static final String NAME = "arch";

    public ArchFormat() {
        super(NAME);
    }
}
