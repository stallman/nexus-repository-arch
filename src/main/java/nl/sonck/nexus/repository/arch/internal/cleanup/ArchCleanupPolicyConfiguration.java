/*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
 */
package nl.sonck.nexus.repository.arch.internal.cleanup;

import com.google.common.collect.ImmutableMap;
import org.sonatype.nexus.cleanup.config.CleanupPolicyConfiguration;
import nl.sonck.nexus.repository.arch.internal.ArchFormat;

import javax.inject.Named;
import javax.inject.Singleton;
import java.util.Map;

import static org.sonatype.nexus.repository.search.DefaultComponentMetadataProducer.*;

/**
 * @since 3.19
 */
@Named(ArchFormat.NAME)
@Singleton
public class ArchCleanupPolicyConfiguration
    implements CleanupPolicyConfiguration
{
  @Override
  public Map<String, Boolean> getConfiguration() {
    return ImmutableMap.of(LAST_BLOB_UPDATED_KEY, true,
        LAST_DOWNLOADED_KEY, true,
        IS_PRERELEASE_KEY, false
        );
  }
}
