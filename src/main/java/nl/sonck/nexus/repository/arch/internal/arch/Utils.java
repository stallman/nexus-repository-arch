package nl.sonck.nexus.repository.arch.internal.arch;

public class Utils {
    private Utils() {}

    public static boolean isArchPackageContentType(final String path) {
        return path.endsWith(".pkg.tar.xz");
    }
}
