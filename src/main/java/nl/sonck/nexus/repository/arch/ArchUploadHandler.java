package nl.sonck.nexus.repository.arch;

import com.google.common.collect.Lists;
import nl.sonck.nexus.repository.arch.internal.ArchPackageParser;
import nl.sonck.nexus.repository.arch.internal.FacetHelper;
import nl.sonck.nexus.repository.arch.internal.arch.InfoFile;
import nl.sonck.nexus.repository.arch.internal.arch.PackageInfo;
import nl.sonck.nexus.repository.arch.internal.hosted.ArchHostedFacet;
import org.sonatype.nexus.repository.Repository;
import nl.sonck.nexus.repository.arch.internal.ArchFormat;
import org.sonatype.nexus.repository.rest.UploadDefinitionExtension;
import org.sonatype.nexus.repository.security.ContentPermissionChecker;
import org.sonatype.nexus.repository.security.VariableResolverAdapter;
import org.sonatype.nexus.repository.storage.Asset;
import org.sonatype.nexus.repository.storage.StorageFacet;
import org.sonatype.nexus.repository.storage.TempBlob;
import org.sonatype.nexus.repository.upload.*;
import org.sonatype.nexus.transaction.UnitOfWork;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Singleton
@Named(ArchFormat.NAME)
public class ArchUploadHandler extends UploadHandlerSupport {
    private final VariableResolverAdapter variableResolverAdapter;

    private final ContentPermissionChecker contentPermissionChecker;

    private UploadDefinition definition;

    @Inject
    public ArchUploadHandler(@Named("simple") final VariableResolverAdapter variableResolverAdapter,
                             final ContentPermissionChecker contentPermissionChecker,
                             final Set<UploadDefinitionExtension> uploadDefinitionExtensions) {
        super(uploadDefinitionExtensions);
        this.variableResolverAdapter = variableResolverAdapter;
        this.contentPermissionChecker = contentPermissionChecker;
    }

    @Override
    public UploadResponse handle(final Repository repository, final ComponentUpload upload) throws IOException {
        ArchFacet archFacet = repository.facet(ArchFacet.class);
        ArchHostedFacet hostedFacet = repository.facet(ArchHostedFacet.class);
        StorageFacet storageFacet = repository.facet(StorageFacet.class);

        try(TempBlob tempBlob = storageFacet
        .createTempBlob(upload.getAssetUploads().get(0).getPayload(), FacetHelper.hashAlgorithms)) {
            String repoName = upload.getFields().get("repo");
            InfoFile infoFile = ArchPackageParser.parsePackage(tempBlob);
            if(infoFile == null) {
                throw new IOException("Invalid arch package:  no control file");

            }

            String name = infoFile.getField(PackageInfo.Field.PACKAGE_NAME).get(0).value;
            String version = infoFile.getField(PackageInfo.Field.VERSION).get(0).value;
            String architecture = infoFile.getField(PackageInfo.Field.ARCHITECTURE).get(0).value;
            String assetPath = FacetHelper.buildAssetPath(repoName, archFacet, name, version, architecture);

            doValidation(repository, assetPath);

            UnitOfWork.begin(storageFacet.txSupplier());
            try {
                Asset asset = hostedFacet.ingestAsset(repoName, upload.getAssetUploads().get(0).getPayload());
                return new UploadResponse(asset);
            } finally {
                UnitOfWork.end();
            }
        }
    }

    private void doValidation(final Repository repository,
                              final String assetpath) {
        ensurePermitted(repository.getName(), ArchFormat.NAME, assetpath, Collections.emptyMap());
    }

    @Override
    public UploadDefinition getDefinition() {
        if(definition == null) {
            List<UploadFieldDefinition> fields = Lists.newArrayList(new UploadFieldDefinition("repo", false, UploadFieldDefinition.Type.STRING));
            definition = getDefinition(ArchFormat.NAME, false, fields, Collections.emptyList(), null);
        }
        return definition;
    }

    @Override
    public VariableResolverAdapter getVariableResolverAdapter() {
        return variableResolverAdapter;
    }

    @Override
    public ContentPermissionChecker contentPermissionChecker() {
        return contentPermissionChecker;
    }
}
