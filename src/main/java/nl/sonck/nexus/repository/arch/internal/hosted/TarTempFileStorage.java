package nl.sonck.nexus.repository.arch.internal.hosted;

import com.google.common.collect.Maps;
import org.apache.commons.compress.archivers.tar.TarArchiveOutputStream;
import org.apache.commons.io.output.CountingOutputStream;
import org.sonatype.goodies.common.ComponentSupport;
import org.sonatype.nexus.repository.view.payloads.StreamPayload.InputStreamSupplier;

import java.io.IOException;
import java.io.UncheckedIOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPOutputStream;

public class TarTempFileStorage
        extends ComponentSupport
        implements AutoCloseable {
    private final Map<String, FileHolder> holdersByKey = new HashMap<>();

    public TarArchiveOutputStream openOutput(String key) {
        try {
            if(holdersByKey.containsKey(key)) {
                throw new IllegalStateException("Output already opened");
            }
            FileHolder holder = new FileHolder();
            holdersByKey.put(key, holder);
            return new TarArchiveOutputStream(new GZIPOutputStream(holder.fileStream));
        } catch (IOException e) {
            throw new UncheckedIOException(e);
        }
    }

    public Map<String, FileMetadata> getFiles() {
        return Maps.transformValues(holdersByKey, FileMetadata::new);
    }

    public void close() {
        List<Path> notDeletedFiles = new LinkedList<>();

        for (FileHolder holder : holdersByKey.values()) {
            deleteFile(holder.filePath, notDeletedFiles);
        }

        if (!notDeletedFiles.isEmpty()) {
            log.warn("Files were not successfully deleted: " + notDeletedFiles);
        }
    }

    private void deleteFile(final Path path, final List<Path> paths) {
        try {
            Files.deleteIfExists(path);
        } catch (IOException e) {
            paths.add(path);
        }
    }

    public static class FileMetadata {
        private final FileHolder holder;

        private FileMetadata(final FileHolder holder) {
            this.holder = holder;
        }

        public long size() {
            return holder.fileStream.getByteCount();
        }

        public InputStreamSupplier supplier() {
            return () -> Files.newInputStream(holder.filePath);
        }
    }

    private static class FileHolder {
        final CountingOutputStream fileStream;

        final Path filePath;

        public FileHolder() throws IOException {
            super();
            this.filePath = Files.createTempFile("", "");
            this.fileStream = new CountingOutputStream(Files.newOutputStream(filePath));
        }
    }
}
