package nl.sonck.nexus.repository.arch.internal;

import org.apache.commons.compress.archivers.ArchiveEntry;
import org.apache.commons.compress.archivers.tar.TarArchiveInputStream;
import org.apache.commons.compress.compressors.xz.XZCompressorInputStream;
import org.apache.commons.io.input.CloseShieldInputStream;
import org.sonatype.nexus.blobstore.api.Blob;
import nl.sonck.nexus.repository.arch.internal.arch.InfoFile;
import nl.sonck.nexus.repository.arch.internal.arch.InfoFileParser;

import java.io.IOException;
import java.io.InputStream;
import java.util.function.Supplier;

public class ArchPackageParser {
    private ArchPackageParser() {
        throw new IllegalAccessError("Utility class");
    }

    public static InfoFile parsePackage(final Supplier<InputStream> supplier) throws IOException {
        try(TarArchiveInputStream is = new TarArchiveInputStream(new XZCompressorInputStream(supplier.get()))) {
            InfoFile info = null;
            ArchiveEntry infoEntry;

            while((infoEntry = is.getNextEntry()) != null) {
                InputStream infoStream;
                switch(infoEntry.getName()) {
                    case ".PKGINFO":
                        infoStream = new CloseShieldInputStream(is);
                        break;
                    default:
                        continue;
                }

                {
                    info = new InfoFileParser().parseInfoFile(infoStream);
                }
            }

            return info;
        }
    }

    public static InfoFile getArchInfoFile(final Blob blob)
        throws IOException
    {
        final InfoFile infoFile = ArchPackageParser.parsePackage(blob::getInputStream);
        if(infoFile == null) {
            throw new IOException("Invalid arch package: no info file");
        }
        return infoFile;
    }
}
