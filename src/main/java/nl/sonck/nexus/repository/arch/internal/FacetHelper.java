/*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
 */
package nl.sonck.nexus.repository.arch.internal;

import org.sonatype.nexus.blobstore.api.Blob;
import org.sonatype.nexus.common.hash.HashAlgorithm;
import nl.sonck.nexus.repository.arch.ArchFacet;
import org.sonatype.nexus.repository.storage.Asset;
import org.sonatype.nexus.repository.view.Content;
import org.sonatype.nexus.repository.view.payloads.BlobPayload;

import java.util.Arrays;
import java.util.List;

import static org.sonatype.nexus.common.hash.HashAlgorithm.MD5;
import static org.sonatype.nexus.common.hash.HashAlgorithm.SHA256;

/**
 * @since 3.17
 */
public class FacetHelper {
    public static final List<HashAlgorithm> hashAlgorithms = Arrays.asList(MD5, SHA256);

    private static final String ASSET_PATH = "%1$s/os/%2$s/%3$s";

    public static Content toContent(final Asset asset, final Blob blob) {
        final Content content = new Content(new BlobPayload(blob, asset.requireContentType()));
        Content.extractFromAsset(asset, hashAlgorithms, content.getAttributes());
        return content;
    }

    public static String buildAssetName(final String packageName, final String version, final String architecture) {
        return packageName + "-" + version + "-" + architecture + ".pkg.tar.xz";
    }

    public static String buildAssetPath(final String repoName, ArchFacet facet, final String packageName, final String version, final String architecture) {
        String repoArch = facet.getArchitecture();

        return String.format(ASSET_PATH, repoName, repoArch,
                buildAssetName(packageName, version, architecture));
    }

    private FacetHelper() {
        //empty
    }
}
