/*
 * Nexus APT plugin.
 * 
 * Copyright (c) 2016-Present Michael Poindexter.
 * 
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * "Sonatype" and "Sonatype Nexus" are trademarks of Sonatype, Inc.
 */

package nl.sonck.nexus.repository.arch.internal.ui;

import org.sonatype.nexus.rapture.UiPluginDescriptorSupport;

import javax.annotation.Priority;
import javax.inject.Named;
import javax.inject.Singleton;

@Named
@Singleton
@Priority(Integer.MAX_VALUE - 200)
public class UiPluginDescriptorImpl
    extends UiPluginDescriptorSupport
{
  public UiPluginDescriptorImpl() {
    super("nexus-repository-arch");
    setNamespace("NX.archui");
    setConfigClassName("NX.archui.app.PluginConfig");
  }
}
