package nl.sonck.nexus.repository.arch.internal.arch;

import com.google.common.base.Charsets;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class MetaFileParser {
    private static final Pattern FIELD_NAME_PATTERN = Pattern.compile("%([^%]+)%");

    private final List<MetaFile.MetaField> fields = new ArrayList<>();

    private final StringBuilder valueBuilder = new StringBuilder();

    private final StringBuilder sigBuilder = new StringBuilder();

    private boolean inField = false;

    private String fieldName;

    public MetaFile parseInfoFile(final InputStream stream) throws IOException {
        fields.clear();
        valueBuilder.setLength(0);
        sigBuilder.setLength(0);
        inField = false;

        BufferedReader reader = new BufferedReader(new InputStreamReader(stream, Charsets.UTF_8));
        String line;

        while((line = reader.readLine()) != null) {
            if(line.trim().length() == 0) {
                finishField();
                continue;
            }

            int first = line.codePointAt(0);
            if(first == '#')
                continue;

            if(inField) {
                valueBuilder.append(line);
            } else if(first == '%') {
                beginField(line);
            }
        }

        finishField();

        return new MetaFile(fields);
    }

    private void finishField() {
        if(!inField) {
            return;
        }
        fields.add(new MetaFile.MetaField(fieldName, valueBuilder.toString()));
        valueBuilder.setLength(0);
        inField = false;
    }

    private void beginField(final String line) throws IOException {
        Matcher m = FIELD_NAME_PATTERN.matcher(line);
        if(!m.matches()) {
            throw new IOException("Invalid line: "+line);
        }
        fieldName = m.group(1).trim();
        inField = true;
    }
}
