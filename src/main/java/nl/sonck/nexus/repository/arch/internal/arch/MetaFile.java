package nl.sonck.nexus.repository.arch.internal.arch;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MetaFile {
    private final List<MetaField> fields;

    public MetaFile(final List<MetaField> fields) {
        super();
        this.fields = new ArrayList<>(fields);
    }

    public MetaFile withFields(List<MetaField> updateFields) {
        Map<PackageInfo.Field, MetaField> index = updateFields.stream().collect(Collectors.toMap(f -> f.key, f -> f));
        return new MetaFile(Stream
                .concat(fields.stream().filter(f -> !index.containsKey(f.key)), updateFields.stream())
                .collect(Collectors.toList())
        );
    }

    @Override
    public String toString() {
        return fields.stream().map(MetaField::toString).collect(Collectors.joining("\n\n"));
    }

    public static class MetaField {

        public final PackageInfo.Field key;

        public final String value;

        public MetaField(final PackageInfo.Field key, final String value) {
            super();
            this.key = key;
            this.value = value;
        }

        public MetaField(final String key, final String value) {
            this(PackageInfo.Field.fromMeta(key).orElseThrow(IllegalArgumentException::new), value);
        }

        List<String> listValue()  {
            return Arrays.asList(value.trim().split("\\s+"));
        }

        @Override
        public String toString() {
            return String.format("%%%s%%\n%s", key.meta, value);
        }
    }
}
