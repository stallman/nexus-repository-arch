package nl.sonck.nexus.repository.arch.internal;

import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.Iterables;
import org.sonatype.nexus.common.collect.AttributesMap;
import org.sonatype.nexus.repository.FacetSupport;
import nl.sonck.nexus.repository.arch.ArchFacet;
import nl.sonck.nexus.repository.arch.internal.arch.PackageInfo;
import org.sonatype.nexus.repository.config.Configuration;
import org.sonatype.nexus.repository.config.ConfigurationFacet;
import org.sonatype.nexus.repository.storage.*;
import org.sonatype.nexus.repository.transaction.TransactionalDeleteBlob;
import org.sonatype.nexus.repository.transaction.TransactionalStoreBlob;
import org.sonatype.nexus.repository.transaction.TransactionalTouchBlob;
import org.sonatype.nexus.repository.types.HostedType;
import org.sonatype.nexus.repository.types.ProxyType;
import org.sonatype.nexus.repository.view.Content;
import org.sonatype.nexus.repository.view.Payload;
import org.sonatype.nexus.transaction.UnitOfWork;

import javax.annotation.Nullable;
import javax.inject.Named;
import javax.validation.constraints.NotNull;
import javax.validation.groups.Default;
import java.io.IOException;

import static java.util.Collections.singletonList;
import static nl.sonck.nexus.repository.arch.internal.arch.Utils.isArchPackageContentType;
import static org.sonatype.nexus.repository.storage.ComponentEntityAdapter.P_GROUP;
import static org.sonatype.nexus.repository.storage.ComponentEntityAdapter.P_VERSION;
import static org.sonatype.nexus.repository.storage.MetadataNodeEntityAdapter.P_NAME;
import static org.sonatype.nexus.repository.storage.Query.builder;


@Named
public class ArchFacetImpl
        extends FacetSupport implements ArchFacet {
    @VisibleForTesting
    static final String CONFIG_KEY = "arch";

    @VisibleForTesting
    static class Config {
        @NotNull(groups = {HostedType.ValidationGroup.class, ProxyType.ValidationGroup.class})
        public String architecture;
    }

    private Config config;

    @Override
    protected void doValidate(final Configuration configuration) throws Exception {
        facet(ConfigurationFacet.class).validateSection(configuration, CONFIG_KEY, Config.class, Default.class, getRepository().getType().getValidationGroup());
    }

    @Override
    protected void doInit(final Configuration configuration) throws Exception {
        super.doInit(configuration);
        getRepository().facet(StorageFacet.class).registerWritePolicySelector(new ArchWritePolicySelector());
    }

    @Override
    protected void doConfigure(final Configuration configuration) throws Exception {
        config = facet(ConfigurationFacet.class).readSection(configuration, CONFIG_KEY, Config.class);
    }

    @Override
    protected void doDestroy() throws Exception {
        config = null;
    }

    @Override
    public String getArchitecture() { return config.architecture; }

    @Override
    @Nullable
    @TransactionalTouchBlob
    public Content get(final String path) throws IOException {
        final StorageTx tx = UnitOfWork.currentTx();
        final Asset asset = tx.findAssetWithProperty(P_NAME, path, tx.findBucket(getRepository()));
        if (asset == null) {
            return null;
        }

        return FacetHelper.toContent(asset, tx.requireBlob(asset.requireBlobRef()));
    }

    @Override
    @TransactionalStoreBlob
    public Content put(final String path, final Payload content) throws IOException {
        return put(path, content, null);
    }

    @Override
    @TransactionalStoreBlob
    public Content put(final String path, final Payload content, final PackageInfo info) throws IOException {
        StorageFacet storageFacet = facet(StorageFacet.class);
        try (final TempBlob tempBlob = storageFacet.createTempBlob(content, FacetHelper.hashAlgorithms)) {
            StorageTx tx = UnitOfWork.currentTx();
            Asset asset = isArchPackageContentType(path)
                    ? findOrCreateArchAsset(tx, path,
                    info != null ? info : new PackageInfo(ArchPackageParser.getArchInfoFile(tempBlob.getBlob())))
                    : findOrCreateMetadataAsset(tx, path);

            AttributesMap contentAttributes = null;
            if (content instanceof Content) {
                contentAttributes = ((Content) content).getAttributes();
            }
            Content.applyToAsset(asset, Content.maintainLastModified(asset, contentAttributes));
            AssetBlob blob = tx.setBlob(
                    asset,
                    path,
                    tempBlob,
                    FacetHelper.hashAlgorithms,
                    null,
                    content.getContentType(),
                    false
            );
            tx.saveAsset(asset);
            return FacetHelper.toContent(asset, blob.getBlob());
        }
    }

    @Override
    public Asset findOrCreateArchAsset(final StorageTx tx, final String path, final PackageInfo packageInfo) {
        Bucket bucket = tx.findBucket(getRepository());
        Asset asset = tx.findAssetWithProperty(P_NAME, path, bucket);
        if (asset == null) {
            Component component = findOrCreateComponent(
                    tx,
                    bucket,
                    packageInfo
            );
            asset = tx.createAsset(bucket, component).name(path);
        }

        return asset;
    }

    @Override
    public Asset findOrCreateMetadataAsset(final StorageTx tx, final String path) {
        Bucket bucket = tx.findBucket(getRepository());
        Asset asset = tx.findAssetWithProperty(P_NAME, path, bucket);
        return asset != null
                ? asset
                : tx.createAsset(bucket, getRepository().getFormat()).name(path);
    }

    private Component findOrCreateComponent(final StorageTx tx, final Bucket bucket, final PackageInfo info) {
        String name = info.getPackageName();
        String version = info.getVersion();
        String architecture = info.getArchitecture();

        Iterable<Component> components = tx.findComponents(
                builder()
                        .where(P_NAME).eq(name)
                        .and(P_VERSION).eq(version)
                        .and(P_GROUP).eq(architecture)
                        .build(),
                singletonList(getRepository())
        );

        Component component = Iterables.getFirst(components, null);
        if (component == null) {
            component = tx.createComponent(bucket, getRepository().getFormat())
                    .name(name)
                    .version(version)
                    .group(architecture);
            tx.saveComponent(component);
        }

        return component;
    }

    @Override
    @TransactionalDeleteBlob
    public boolean delete(final String path) throws IOException {
        StorageTx tx = UnitOfWork.currentTx();
        Bucket bucket = tx.findBucket(getRepository());
        Asset asset = tx.findAssetWithProperty(P_NAME, path, bucket);
        if (asset == null) {
            return false;
        }

        Component component = tx.findComponent(asset.componentId());

        tx.deleteAsset(asset);
        tx.deleteComponent(component);
        return true;
    }
}
