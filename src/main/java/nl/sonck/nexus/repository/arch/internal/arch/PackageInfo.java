package nl.sonck.nexus.repository.arch.internal.arch;

import java.util.EnumSet;
import java.util.Optional;

public class PackageInfo {
    private final InfoFile infoFile;

    public PackageInfo(final InfoFile infoFile) {
        this.infoFile = infoFile;
    }

    public String getPackageName() {
        return getField(Field.PACKAGE_NAME);
    }

    public String getVersion() {
        return getField(Field.VERSION);
    }

    public String getArchitecture() {
        return getField(Field.ARCHITECTURE);
    }

    private String getField(final Field fieldName) {
        return infoFile.getField(fieldName).get(0).value;
    }

    public enum Field {
        FILENAME(null, "FILENAME"),
        PACKAGE_NAME("pkgname", "NAME"),
        PACKAGE_BASE("pkgbase", "BASE"),
        VERSION("pkgver", "VERSION"),
        DESCRIPTION("pkgdesc", "DESC"),
        URL("url", "URL"),
        BUILD_DATE("builddate", "BUILDDATE"),
        LICENSE("license", "LICENSE"),
        ARCHITECTURE("arch", "ARCH"),
        DEPENDS("depend", "DEPENDS"),
        OPTIONAL_DEPENDS("optdepend", "OPTDEPENDS"),
        MAKE_DEPENDS("makedepend", null),
        CHECK_DEPENDS("checkdepend", null),
        PROVIDES("provides", "PROVIDES"),
        CONFLICTS("conflict", "CONFLICTS"),
        REPLACES("replaces","REPLACES"),
        PACKAGER("packager", "PACKAGER"),
        ISIZE("size", "ISIZE"),
        CSIZE(null,"CSIZE"),
        MD5SUM(null,"MD5SUM"),
        SHA256SUM(null,"SHA256SUM"),
        PGPSIG(null,"PGPSIG"),
        GROUP("group", "GROUPS");

        String info;
        String meta;

        Field(String info, String meta) {
            this.info = info;
            this.meta = meta;
        }

        boolean hasInfo() {
            return info != null;
        }

        boolean hasMeta() {
            return meta != null;
        }

        static EnumSet<Field> all = EnumSet.allOf(Field.class);

        static Optional<Field> fromInfo(String info) {
            return all.stream().filter(f -> info.equals(f.info)).findFirst();
        }

        static Optional<Field> fromMeta(String meta) {
            return all.stream().filter(f -> meta.equals(f.meta)).findFirst();
        }
    }
}
