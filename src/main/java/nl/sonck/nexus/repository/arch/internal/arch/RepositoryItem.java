package nl.sonck.nexus.repository.arch.internal.arch;

import org.sonatype.nexus.repository.Repository;
import org.sonatype.nexus.repository.view.Content;

public class RepositoryItem {
    public static enum Role {
        DB_INDEX, DB_INDEX_GZ, DB_INDEX_GZ_OLD, FILES_INDEX, FILES_INDEX_GZ, FILES_INDEX_GZ_OLD
    }

    public static class ContentSpecifier {
        public final String path;
        public final Role role;

        public ContentSpecifier(final String path, final Role role) {
            super();
            this.path = path;
            this.role = role;
        }
    }

    public final ContentSpecifier specifier;
    public final Content content;

    public RepositoryItem(final ContentSpecifier specifier, Content content) {
        super();
        this.specifier = specifier;
        this.content = content;
    }
}
