/*
 * Sonatype Nexus (TM) Open Source Version
 * Copyright (c) 2008-present Sonatype, Inc.
 * All rights reserved. Includes the third-party code listed at http://links.sonatype.com/products/nexus/oss/attributions.
 *
 * This program and the accompanying materials are made available under the terms of the Eclipse Public License Version 1.0,
 * which accompanies this distribution and is available at http://www.eclipse.org/legal/epl-v10.html.
 *
 * Sonatype Nexus (TM) Professional Version is available from Sonatype, Inc. "Sonatype" and "Sonatype Nexus" are trademarks
 * of Sonatype, Inc. Apache Maven is a trademark of the Apache Software Foundation. M2eclipse is a trademark of the
 * Eclipse Foundation. All other trademarks are the property of their respective owners.
 */
package nl.sonck.nexus.repository.arch.internal.hosted;

import nl.sonck.nexus.repository.arch.internal.ArchFacetImpl;
import nl.sonck.nexus.repository.arch.internal.ArchRecipeSupport;
import nl.sonck.nexus.repository.arch.internal.ArchSecurityFacet;
import org.sonatype.nexus.repository.Format;
import org.sonatype.nexus.repository.Repository;
import org.sonatype.nexus.repository.Type;
import nl.sonck.nexus.repository.arch.ArchRestoreFacet;
import nl.sonck.nexus.repository.arch.internal.ArchFormat;
import nl.sonck.nexus.repository.arch.internal.gpg.ArchSigningFacet;
import nl.sonck.nexus.repository.arch.internal.gpg.ArchSigningHandler;
import org.sonatype.nexus.repository.attributes.AttributesFacet;
import org.sonatype.nexus.repository.http.PartialFetchHandler;
import org.sonatype.nexus.repository.search.SearchFacet;
import org.sonatype.nexus.repository.security.SecurityHandler;
import org.sonatype.nexus.repository.storage.StorageFacet;
import org.sonatype.nexus.repository.storage.UnitOfWorkHandler;
import org.sonatype.nexus.repository.types.HostedType;
import org.sonatype.nexus.repository.view.ConfigurableViewFacet;
import org.sonatype.nexus.repository.view.Route;
import org.sonatype.nexus.repository.view.Router;
import org.sonatype.nexus.repository.view.ViewFacet;
import org.sonatype.nexus.repository.view.handlers.*;
import org.sonatype.nexus.repository.view.matchers.AlwaysMatcher;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Provider;
import javax.inject.Singleton;

import static org.sonatype.nexus.repository.http.HttpHandlers.notFound;

/**
 * @since 3.17
 */
@Named(ArchHostedRecipe.NAME)
@Singleton
public class ArchHostedRecipe
    extends ArchRecipeSupport
{

  public static final String NAME = "arch-hosted";

  @Inject
  Provider<ArchSecurityFacet> securityFacet;

  @Inject
  FormatHighAvailabilitySupportHandler highAvailabilitySupportHandler;

  @Inject
  Provider<ConfigurableViewFacet> viewFacet;

  @Inject
  Provider<ArchFacetImpl> aptFacet;

  @Inject
  Provider<ArchRestoreFacet> aptRestoreFacet;

  @Inject
  Provider<ArchHostedFacet> aptHostedFacet;

  @Inject
  Provider<ArchSigningFacet> aptSigningFacet;

  @Inject
  Provider<StorageFacet> storageFacet;

  @Inject
  Provider<AttributesFacet> attributesFacet;

  @Inject
  Provider<ArchHostedComponentMaintenanceFacet> componentMaintenance;

  @Inject
  Provider<SearchFacet> searchFacet;

  @Inject
  ExceptionHandler exceptionHandler;

  @Inject
  TimingHandler timingHandler;

  @Inject
  SecurityHandler securityHandler;

  @Inject
  PartialFetchHandler partialFetchHandler;

  @Inject
  UnitOfWorkHandler unitOfWorkHandler;

  @Inject
  ArchHostedHandler hostedHandler;

  @Inject
  ConditionalRequestHandler conditionalRequestHandler;

  @Inject
  ContentHeadersHandler contentHeadersHandler;

  @Inject
  ArchSigningHandler signingHandler;

  @Inject
  LastDownloadedHandler lastDownloadedHandler;

  @Inject
  public ArchHostedRecipe(final HighAvailabilitySupportChecker highAvailabilitySupportChecker,
                          @Named(HostedType.NAME) final Type type,
                          @Named(ArchFormat.NAME) final Format format)
  {
    super(highAvailabilitySupportChecker, type, format);
  }

  @Override
  public void apply(final Repository repository) throws Exception {
    repository.attach(securityFacet.get());
    repository.attach(configure(viewFacet.get()));
    repository.attach(storageFacet.get());
    repository.attach(aptFacet.get());
    repository.attach(aptRestoreFacet.get());
    repository.attach(aptHostedFacet.get());
    repository.attach(aptSigningFacet.get());
    repository.attach(attributesFacet.get());
    repository.attach(componentMaintenance.get());
    repository.attach(searchFacet.get());
  }

  private ViewFacet configure(final ConfigurableViewFacet facet) {
    Router.Builder builder = new Router.Builder();

    builder.route(new Route.Builder().matcher(new AlwaysMatcher())
        .handler(timingHandler)
        .handler(securityHandler)
        .handler(highAvailabilitySupportHandler)
        .handler(exceptionHandler)
        .handler(conditionalRequestHandler)
        .handler(partialFetchHandler)
        .handler(contentHeadersHandler)
        .handler(unitOfWorkHandler)
        .handler(lastDownloadedHandler)
        .handler(signingHandler)
        .handler(hostedHandler).create());

    builder.defaultHandlers(notFound());
    facet.configure(builder.create());
    return facet;
  }
}
